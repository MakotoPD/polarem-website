export default {
  head: {
    title: 'Pola-Rem - Remonty i wykończenia wnętrz',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Pola-Rem - Remonty i wykończenia wnętrz.' },
      { name: 'keywords', content: 'pola, rem, polarem, pola-rem, firma, remontowo, budowlana, remonty, wykończenia, wykonczenia, wnętrz, inowroclaw, inowrocław, Inowroclaw, Inowrocław torun, toruń, Torun, Toruń, bydgoszcz, Bydgoszcz, kompleksowo, adaptacje, adaptacja, poddasze, poddaszy, wykonanie, sufity, napinane, Remonty i wykończenia wnętrz inowrocław, kompleksowo wykonane remonty, adaptacja poddaszy, sufity napinane,' },
      { name: 'og:description', content: 'Pola-Rem - Remonty i wykończenia wnętrz. tel.781 953 979 ' },
      { property: 'og:locale', content: 'pl_PL'},
      { property: 'og:title', content: 'Pola-Rem - Remonty i wykończenia wnętrz'},
      { property: 'og:type', content: 'website'},
      { property: 'og:image', content: 'https://polarem-website.vercel.app/wizytowka.jpg'}
    ],
    link: [
      { rel: 'icon', href: '/favicon.png' }
    ]
  },

  css: [
  ],

  plugins: [
  ],

  components: true,

  buildModules: [
    '@nuxtjs/tailwindcss'
  ],

  modules: [
    '@nuxtjs/pwa',
    ['nuxt-lazy-load', {
      images: true,

      loadingClass: 'isLoading',
      loadedClass: 'isLoaded',
      appendClass: 'lazyLoad',
    }]
  ],

  pwa: {
    manifest: {
      lang: 'pl'
    }
  },

  build: {
    
  },
}
